(function ($) {
    $(function () {
        // START custom select js
        if ( $.fn.select2 && $('.select2').length ) {
            $('.form-control.select2').select2({
                theme: 'default',
                language: 'ru',
                minimumResultsForSearch: 10
            });

            $('.form-control.select2').on('select2:open', function () {
                $('.select2-results__options').niceScroll({
                    cursorcolor: '#353535',
                    cursoropacitymin: 0.15,
                    cursoropacitymax: 0.5,
                    cursorwidth: 5,
                    cursorborder: 0,
                    cursorborderradius: 5,
                    railpadding: { top: 0, right: 2, left: 0, bottom: 0 }
                });
            });
        }
        // END custom select js
    });
})(jQuery);
